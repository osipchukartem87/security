<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\DocBlock\Tags\Uses;

class Product extends Model
{
    protected $primaryKey = 'id';
    protected $fillable = [
        'name', 'price', 'user_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\Entity\User');
    }

}
