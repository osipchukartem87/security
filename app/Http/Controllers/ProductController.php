<?php

namespace App\Http\Controllers;

use App\Entity\Product;
use App\Entity\User;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\Console\Input\Input;

class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $products = Product::all();
        return view('products', ['products' => $products]);
    }

    public function store(Request $request)
    {

        $data['name'] = $request->input('name');
        $data['price'] = (float)$request->input('price');
        $data['user_id'] = Auth::user()->id;
        $data['updated_at'] = \DateTime::ISO8601;
        $data['created_at'] = \DateTime::ISO8601;

        $product = Product::create($data);
        $product->save();

        try {
            $this->authorize('update', $product);
        } catch (AuthenticationException $e) {
            return redirect('/products');
        }

        return redirect('/products');
    }

    public function show($id)
    {
        $product = Product::find($id);

        return view('product', ['product' => $product]);
    }

    public function update(Request $request, $id)
    {

        $product = Product::find($id);
        $product->name = $request->input('name');
        $product->price = (float)$request->input('price');
        $product->save();

        return redirect('/products');
    }

    public function edit($id)
    {
        $product = Product::find($id);

        if (!$product) {
            return view('404');
        }

        try {
            $this->authorize('update', $product);
        } catch (AuthenticationException $e) {
            return redirect('/products');
        }

        return view('edit', ['product' => $product]);
    }

    public function destroy($id)
    {
        $product = Product::find($id);

        try {
            $this->authorize('delete', $product);
        } catch (AuthenticationException $e) {
            return redirect('/products');
        }
        $product->destroy($id);

        return redirect('/products');

    }
}
