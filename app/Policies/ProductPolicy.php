<?php

namespace App\Policies;

use App\Entity\User;
use App\Entity\Product;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProductPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any products.
     *
     * @param  \App\Entity\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can view the product.
     *
     * @param  \App\Entity\User  $user
     * @param  \App\Entity\Product  $product
     * @return mixed
     */
    public function view(User $user, Product $product)
    {
        return $this->isUsersProduct($user, $product) || $user->isAdmin();
    }

    /**
     * Determine whether the user can create products.
     *
     * @param  \App\Entity\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can update the product.
     *
     * @param  \App\Entity\User  $user
     * @param  \App\Entity\Product  $product
     * @return mixed
     */
    public function update(User $user, Product $product)
    {
        return $this->isUsersProduct($user, $product) || $user->isAdmin();
    }

    /**
     * Determine whether the user can delete the product.
     *
     * @param  \App\Entity\User  $user
     * @param  \App\Entity\Product  $product
     * @return mixed
     */
    public function delete(User $user, Product $product)
    {
        return $this->isUsersProduct($user, $product) || $user->isAdmin();
    }

    /**
     * Determine whether the user can restore the product.
     *
     * @param  \App\Entity\User  $user
     * @param  \App\Entity\Product  $product
     * @return mixed
     */
    public function restore(User $user, Product $product)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the product.
     *
     * @param  \App\Entity\User  $user
     * @param  \App\Entity\Product  $product
     * @return mixed
     */
    public function forceDelete(User $user, Product $product)
    {
        //
    }

    private function isUsersProduct($user, $product)
    {
        return $user->id === $product->user_id;
    }
}
