<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Entity\Product' => 'App\Policies\ProductPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();


       Gate::define('edit', function ($user) {
           return $user->isAdmin();
        });

        Gate::define('edit', function ($user, $product) {
            return $user->id === $product->user_id || $user->is_admin;
        });

        Gate::define('delete', function ($user, $product) {
            return $user->id === $product->user_id || $user->is_admin;
        });
    }
}
