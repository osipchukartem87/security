@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit</div>

                <div class="card-body">
                    <form method="POST" action="{{ url('/products').'/'.$product->id }}">
                        @method('PATCH')
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="name">Product name</label>
                            <input name="name" type="text" value="{{ $product->name }}" required>
                        </div>
                        <div class="form-group">
                            <label for="price">Price</label>
                            <input name="price" type="number" value="{{ $product->price }}" required>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
