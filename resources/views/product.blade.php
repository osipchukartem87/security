@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ $product->name }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div>{{ $product->id }}</div>
                    <div>{{ $product->name }}</div>
                    <div>{{ $product->price }}</div>
                    <div>{{ $product->user_id }}</div>

                        @can(['edit', 'delete'], $product)
                            <a href="{{ url('/products').'/'.$product->id.'/edit' }}">Edit</a>
                            <form action="{{ url('/products').'/'.$product->id }}" method="POST">
                                @method('DELETE')
                                @csrf
                                <button type="submit">Delete</button>
                            </form>
                        @endcan

                </div>

            </div>
        </div>
    </div>
</div>
@endsection
