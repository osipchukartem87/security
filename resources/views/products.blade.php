@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Products</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                        <form method="POST" action="{{ url('/products/add') }}">
                            @method('POST')
                            {{ csrf_field() }}

                            <div class="form-group">
                                <label for="name">Product name</label>
                                <input name="name" type="text" value="" required>
                            </div>
                            <div class="form-group">
                                <label for="price">Price</label>
                                <input name="price" type="number" value="" required>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Add</button>
                            </div>
                        </form>
                    <ul>
                        @foreach($products as  $product)
                            <li><a href="{{ url('/products').'/'.$product->id }}" style="text-decoration:none">{{ $product->id }} {{ $product->name }}</a></li>
                        @endforeach
                    </ul>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
