<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/products', 'ProductController@index');
Route::get('/products/{id}', 'ProductController@show');

Route::post('/products/add', 'ProductController@store');

Route::get('/products/{id}/edit', 'ProductController@edit');
Route::patch('/products/{id}', 'ProductController@update');

Route::delete('/products/{id}', 'ProductController@destroy');

Route::get('login/facebook', 'SocialAuthController@redirectToProvider');
Route::get('login/facebook/callback', 'SocialAuthController@handleProviderCallback');
